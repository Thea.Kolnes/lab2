package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    public ArrayList<FridgeItem> fridgeItems = new ArrayList<>(); // makes new arraylist
    
    @Override
    public int nItemsInFridge() {
        return this.fridgeItems.size(); // counts elements in arraylist
    }

    @Override
    public int totalSize() {
        int maxSize = 20;
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() >= totalSize()) {
            return false;
        }
        return this.fridgeItems.add(item); // remember to add!
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge() == 0){
            throw new NoSuchElementException("No items to throw."); // uses exception
        } else {
            this.fridgeItems.remove(item); // removes item
        }
    }

    @Override
    public void emptyFridge() {
        this.fridgeItems.clear(); // .clear() empties the whole array
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : fridgeItems){ // for each item in fridgeItems
            if (item.hasExpired()) {  // uses method from FridgeItem
                expiredFood.add(item);
            }
        }
        for (FridgeItem expiredItem : expiredFood) { // checks elements in expiredFood array
            this.fridgeItems.remove(expiredItem);
        }
        return expiredFood;
    }

}
